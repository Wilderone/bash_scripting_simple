# simple arithmetics

a=$(( 2 + 2 ))
echo $a
echo $(( 3 % 2 ))

# expr
expr 2 + 2 # spaces around sign and escape * with \
expr 5 - 2
expr 3 \* 2

#let - do math and save it in variable

let a=2+2
let b="4*($a-1)"
let b--
let b++

#bc - strong instrument for advanced math
echo '8.5 / 2.3' | bc
echo 'scale=2;8.5 / 2.3' | bc
squareroot=$( echo 'scale=50;sqrt(50)' | bc )

