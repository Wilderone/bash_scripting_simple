#!/usr/bin/env bash

#user=$(whoami)
function backup {
echo "arg1 is $1"
echo "arg2 is $2"
if [ -z $1 ]; then
	user=$(whoami)
else
	if [ ! -d "/home/$1" ]; then
		echo "$1 user home dir doesnt exist."
		exit 1
	fi
	user=$1
	
fi

filename="backup_${user}_home_$(date +%Y-%m-%d_%H%M%S)"

#if [ -z $2 ]; then
#	output=/tmp/${user}_${filename}
#else
#	if [ ! -d $2 ]; then
#	       echo "path $2 does not exists, creating this path"
#       		mkdir -p $2
# 	fi
#	output=$2/${filename}
#fi	

input=/home/$user
output=/tmp/${user}_home_$(date +%Y-%m-%d_%H%M%S).tar.gz


function total_files {

	find $1 -type f | wc -l
	}

function total_dirs {
	find -type d | wc -l
	}



function total_archived_files {
	tar -tzf $1 | grep -v /$ | wc -l

	}
function total_archived_dirs {
	tar -tzf $1 | grep /$ | wc -l
	}

tar -czf $output $input 2> /dev/null

src_files=$( total_files $input )
src_dirs=$( total_dirs $input )

arch_files=$( total_archived_files $output )
arch_dirs=$( total_archived_dirs $output )



echo "############## $user ###############"
echo "backup of $input for $user created"



echo -n "Files to be included:"
total_files $input
echo -n  "Directories to be included:"
total_dirs $input

echo "Files archived: $arch_files"
echo "Dirs archived: $arch_dirs"
if [ $src_files -eq $arch_files ]; then
	echo "Backup of $input complete!"
	ls -l $output
else
	echo "Backup of $input failed"
fi
}

for username in $*; do
	backup $username
done;
