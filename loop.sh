#!/usr/bin/env bash

files=$( ls -a)

for i in $files; do
	for word in $i; do
		echo -n "$i has "
	        echo -n $word | wc -c	
	done
done
