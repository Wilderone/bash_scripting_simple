#!/usr/bin/env bash

greeting="Welcome!"
user=$(whoami)
day=$(date +%A)

echo "$greeting back $user! Today is $day."
echo "Bash shell version is $BASH_VERSION"

